const form = document.querySelector(".form");
const buttonInput = document.querySelector(".form__btn-input");
const inputEls = document.querySelectorAll(".form__input-el");

let staf = [];
let user = {
  show() {
    console.log(this);
  },
};
// обробник подій
form.addEventListener("input", getDateFromUser);
form.addEventListener("submit", addUserDate);
// запис введених даних
function getDateFromUser(e) {
  user[e.target.id] = e.target.value;
  user.show();
}
// запис введених даних в localStorage
function addUserDate() {
  if (localStorage.getItem("users")) {
    staf = JSON.parse(localStorage.getItem("users"));
  }

  staf.push(user);
  localStorage.setItem("users", JSON.stringify(staf));
  form.reset();
}
