const url = `https://api.nasa.gov/planetary/apod?api_key=FpdmDYgcPYRb8wtCJxpMBtwTZumyXMHW74t1tQUR`;

const image = document.querySelector('.picofday');
const caption = document.querySelector('.pic');

fetch(url)
  .then(response => {
  return response.json();
})
  .then(data => {
  console.log(data);
  image.src = data.url;
  caption.innerHTML = data.explanation;
});
